/*Faça um programa que receba o salário-base de um funcionário, calcule e mostre o salário a receber, sabendo-se que esse funcionário tem gratificação de 5% sobre o salário
base e paga imposto de 7% sobre o salário-base.
*/

//Inicio
namespace exercicio_6 

{//Entrada de dados
    let salariOBase: number;
    let gratificacao:number;
    let imposto:number;

    salariOBase =1200 
    
    let novOSalario: number; 

    //Processamento de dados
    gratificacao= salariOBase * 5/100
    imposto= salariOBase * 7/100
    novOSalario = salariOBase + gratificacao - imposto

    //Saida
    console.log(`O resultado do aumento é ${gratificacao} \n O resultado do desconto é ${imposto} \n O resultado do salario a receber é ${novOSalario}`);

}