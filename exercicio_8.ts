/*Faça um programa que receba o valor de um depósito e o valor da taxa de juros, calcule e mostre o valor do rendimento e o valor total depois do rendimento.
*/

//Inicio
namespace exercicio_8
{
    //Entrada de dados
    let deposito:number;
    let taxAJuros:any ;

    deposito = 1200
    taxAJuros = 10

    let rendimento:number; 
    let valoRTotal: number;

    //Processamento de dados
    rendimento= deposito * taxAJuros/100
    valoRTotal= deposito + rendimento

    //Saida
    console.log(`O resultado do aumento é ${rendimento} \n O resultado do total é ${valoRTotal}`);
}