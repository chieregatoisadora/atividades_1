/*Faça um programa que receba o salário de um funcionário e o percentual de aumento, calcule e mostre o valor do aumento e o novo salário.
*/

//Inicio
namespace exercicio_5

{//Entrada de dados
    let salariOBase: number; 
    let percentual:any;

    salariOBase= 1200
    percentual = 10

    let aumento: number; 
    let salariOTotal: number; 

    //Processamento de dados 
    aumento = (salariOBase * percentual /100)
    salariOTotal = salariOBase + aumento

    //Saida
    console.log(`O resultado do percentual é ${aumento} `);
    console.log(`O resultado do salario é ${salariOTotal}`);
        
}