

//Inicio
namespace exercicio_11 {
  //Entrada de dados
  const numero = 2;

  let numQ: number;
  numQ = Math.pow(numero, 2);

  let numC: number;
  numC = Math.pow(numero, 3);

  let raizQ: number;
  raizQ = Math.sqrt(numero);

  let raizC: number;
  raizC = Math.cbrt(numero);

  //Saida
  console.log(
    `O numero elevado ao quadrado:${numQ} \n O numero elevado ao cubo: ${numC} \n A raiz quadrada do numero:${raizQ} \n A raiz cubica do numero: ${raizC}`
  );
}
