/* Faça um programa que receba três notas, calcule e mostre a média ponderada entre elas.
*/ 
// Inicio 

namespace exercicio_3
 {
    //Entrada de dados
    let nota1: number;
    let nota2: number; 
    let nota3: number; 
    let peso1: number; 
    let peso2: number; 
    let peso3: number; 

    nota1 = 7;
    nota2 = 10;
    nota3 = 5;
    peso1 = 2;
    peso2 = 5;
    peso3 = 3;

    let media_p : number;

    //Processo de dados
    media_p = (nota1 * peso1 + nota2 * peso2 + nota3 *peso3) /(peso1 + peso2 + peso3)
 
    //Saida
    console.log(`O resultado da nota é ${media_p}`);
 }


 