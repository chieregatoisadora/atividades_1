/*Faça um programa que calcule e mostre a área de um triângulo. Sabe-se que: Área = (base * altura)/2.
*/

//Inicio
namespace exercicio_9
{
    //Entrada de dados
    let base: number;
    let altura: number;

    base= 10
    altura= 8

    let area:number; 

    //Processamento de dados
    area= (base * altura)/2

    //Saida
    console.log(`O resultado do triangulo é ${area}`);

}