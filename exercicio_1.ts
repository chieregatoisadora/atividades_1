//Comentar 
/*
Faça um programa que receba quatro numeros inteiros,calcule e mostre a soma desses numeros
*/

//Primeiro bloco
//Inicio
namespace exercicio_1
{
    //Entrada dos dados
    //var --- let ---const
    let numero1:number;
    let numero2:number;
    let numero3:number;
    let numero4:number; 
    
    numero1 = 6;
    numero2 = 7;
    numero3 = 128;
    numero4 = 15;
    
    let resultado:number;

    //Processar os dados
    resultado = numero1 + numero2 + numero3 + numero4;

    //saida
    console.log("O resultado da soma é:"
            + resultado + "\n")
    //Ou pode-se escrever
    console.log(`O resultado da soma é:$ {resultado}`);
    
}